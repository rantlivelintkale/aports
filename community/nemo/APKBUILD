# Contributor: Clayton Craft <clayton@craftyguy.net>
# Maintainer:
pkgname=nemo
pkgver=5.8.2
pkgrel=0
pkgdesc="File manager for the Cinnamon desktop environment"
# s390x and ppc64le blocked by exempi
# riscv64 disabled due to missing rust in recursive dependency
arch="all !s390x !ppc64le !riscv64"  # exempi, libexif-dev not available for all archs
url="https://github.com/linuxmint/nemo"
license="GPL-2.0-only"
makedepends="
	cinnamon-desktop-dev
	dconf-dev
	exempi-dev
	gobject-introspection-dev
	gvfs-dev
	intltool
	libexif-dev
	libgsf-dev
	libnotify-dev
	libxml2-dev
	meson
	python3
	xapp-dev
	"
checkdepends="xvfb-run"
source="https://github.com/linuxmint/nemo/archive/$pkgver/nemo-$pkgver.tar.gz"
subpackages="$pkgname-doc $pkgname-dev"
options="!check" # tests are broken: https://github.com/linuxmint/nemo/issues/2501

build() {
	abuild-meson . output
	meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

check() {
	xvfb-run meson test -C output
}
sha512sums="
fb69acad59393c64e3ef2f5e3f0c2c219a2a58a500f2dfefacfed58ad194df7d082c5b37588f58c1fc668ea577c7b2cc795c5a3ad9fe69494cfbbddcbd0d1769  nemo-5.8.2.tar.gz
"
