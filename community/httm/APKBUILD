# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=httm
pkgver=0.29.4
pkgrel=0
pkgdesc="Interactive, file-level Time Machine-like tool for ZFS/btrfs"
url="https://github.com/kimono-koans/httm"
# riscv64: TODO
# s390x: fails to build nix crate
arch="all !riscv64 !s390x"
license="MPL-2.0"
makedepends="cargo acl-dev cargo-auditable"
subpackages="$pkgname-doc"
source="https://github.com/kimono-koans/httm/archive/refs/tags/$pkgver/httm-$pkgver.tar.gz"
options="net !check"  # no tests provided

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --features acls
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/
	install -D -m644 $pkgname.1 -t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
6b88c51756efdf8f94f620ebf7b87b01fad7ecd612c3eb82fb4eb2cefd781b065fa72f77a9aacb549a3eb69a32191605b5724433edcad880c4dcee4c78c0dbd0  httm-0.29.4.tar.gz
"
