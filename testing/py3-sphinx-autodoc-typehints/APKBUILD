# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-sphinx-autodoc-typehints
_pyname=sphinx-autodoc-typehints
pkgver=1.23.2
pkgrel=0
pkgdesc="Type hints support for the Sphinx autodoc extension"
url="https://github.com/tox-dev/sphinx-autodoc-typehints"
arch="noarch"
license="MIT"
options="net"
depends="python3 py3-sphinx py3-typing-extensions"
makedepends="py3-gpep517 py3-installer py3-hatchling py3-hatch-vcs"
checkdepends="py3-pytest py3-sphobjinv py3-nptyping"
subpackages="$pkgname-pyc"
source="$_pyname-$pkgver.tar.gz::https://github.com/tox-dev/sphinx-autodoc-typehints/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages test-env
	test-env/bin/python3 -m installer .dist/sphinx_autodoc_typehints*.whl
	test-env/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/sphinx_autodoc_typehints*.whl
}

sha512sums="
c4a59073a7c752fcf69d702f112e7180e59fc0da0870f631b5a6f287410a405d8f1359b99486c8ed7c2f058a803933c381d70fb11877a88ff0c9d3870c4a0007  sphinx-autodoc-typehints-1.23.2.tar.gz
"
